@extends('layouts.main')

@section('title')
    Liste des pays
@endsection

@section('active1')
    nav-item
@endsection

@section('active2')
    nav-item active
@endsection

@section('active3')
    nav-item
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-plain">
        <div class="card-header card-header-primary">
            <h4 class="card-title mt-0"> Tableau des pays</h4>
            <p class="card-category"> Vous trouverez les informations des pays ci-dessous</p>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-hover">
                <thead class="">
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Libelle
                        </th>
                        <th>
                            Description
                        </th>
                        <th>
                            Code Indicatif
                        </th>
                        <th>
                            Continent
                        </th>
                        <th>
                            Population
                        </th>
                        <th>
                            Capitale
                        </th>
                        <th>
                            Monnaie
                        </th>
                        <th>
                            Langue
                        </th>
                        <th>
                            Superficie
                        </th>
                        <th>
                            Est laique
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pays as $ipays)
                    <tr>
                        <td>{{$ipays->id}}</td>
                        <td>{{$ipays->libelle}}</td>
                        <td>{{$ipays->description}}</td>
                        <td>{{$ipays->code_indicatif}}</td>
                        <td>{{$ipays->continent}}</td>
                        <td>{{$ipays->population}}</td>
                        <td>{{$ipays->capitale}}</td>
                        <td>{{$ipays->monnaie}}</td>
                        <td>{{$ipays->langue}}</td>
                        <td>{{$ipays->superficie}}</td>
                        <td>
                            @if ($ipays->est_laique == 1)
                                Oui
                            @else
                                Non
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
