@extends('layouts.main')

@section('title')
    Ajouter un pays
@endsection

@section('active1')
    nav-item
@endsection

@section('active2')
    nav-item
@endsection

@section('active3')
    nav-item active
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Formulaire d'ajout</h4>
                <p class="card-category">Remplissez les informations</p>
              </div>
              <div class="card-body">
                <form action="{{route('pays.store')}}" method="POST">
                    @method("POST")
                    @csrf <!-- Empeche injection sql/ pour la securité -->
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating">Libelle</label>
                        <input type="text" name = "libelle" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating">Superficie</label>
                        <input type="number" name = "superficie" class="form-control">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating">Code indicatif</label>
                        <input type="text" name = "code" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label class="">Continent</label>
                        <select name="continent" id="">
                            <option value="Afrique">Afrique</option>
                            <option value="Europe">Europe</option>
                            <option value="Asie">Asie</option>
                            <option value="Amérique">Amérique</option>
                            <option value="Océanie">Océanie</option>
                            <option value="Antarctique">Antarctique</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating">Population</label>
                        <input type="number" name="population" class="form-control">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating">Capitale</label>
                        <input type="text" name="capitale" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group bmd-form-group">
                        <label class="">Monnaie</label>
                        <select name="monnaie" id="" required>
                            <option value="XOF">XOF</option>
                            <option value="EUR">EUR</option>
                            <option value="DOLLAR">DOLLAR</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group bmd-form-group">
                        <label class="">Langue</label>
                        <select name="langue" id="">
                            <option value="FR">FR</option>
                            <option value="EN">EN</option>
                            <option value="AR">AR</option>
                            <option value="ES">ES</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                          <label class="">Est laique</label>
                          <select name="laique" id="">
                              <option value="1">Oui</option>
                              <option value="0">Non</option>
                          </select>
                        </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Description</label>
                        <div class="form-group bmd-form-group">
                          <label class="bmd-label-floating"> Entrez la description du pays</label>
                          <textarea class="form-control" name = "description" rows="5"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Enregistrer</button>
                  <div class="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
    </div>
@endsection
