<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{asset("assets/img/sidebar-1.jpg")}}">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo"><a href="http://www.creative-tim.com" class="simple-text logo-normal">
        Pays Manager 2.0
      </a></div>
    <div class="sidebar-wrapper">
      <ul class="nav">
          <!-- Les yield active permettent de changer la surbrillance de la page en cours dans la sidebar-->
        <li class="@yield('active1')">
          <a class="nav-link" href="{{url('/')}}">
            <i class="material-icons">dashboard</i>
            <p>Accueil</p>
          </a>
        </li>
        <li class="@yield('active2')">
          <a class="nav-link" href="{{url('/pays')}}">
            <i class="material-icons">content_paste</i>
            <p>Liste des pays</p>
          </a>
        </li>
        <li class="@yield('active3')">
          <a class="nav-link" href="{{url('/pays/create')}}">
            <i class="material-icons">add</i>
            <p>Ajouter un pays</p>
          </a>
        </li>
      </ul>
    </div>
  </div>
